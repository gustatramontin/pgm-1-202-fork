class Carro:

    def __init__(self, nome: str, ano: int) -> None:
        self.i = 1
        self.nome = nome
        self.ano = ano
        self.direcao = None

    def mudar_ano(self, novo_ano: int) -> None:
        if novo_ano > 1886:
            self.ano = novo_ano

    def pegar_ano(self) -> int:
        return self.ano

    def definir_direcao(self, nova_direcao: str) -> None:
        self.direcao = nova_direcao
        print(self.direcao)


if __name__ == "__main__":

    fusca = Carro("Fusca", 1989)
    uno = Carro("Uno", 1994)
    """
    direcao = input("Digite uma direção para o fusca: ")
    fusca.definir_direcao(direcao)

    direcao = input("Digite uma direção para o Uno: ")
    uno.definir_direcao(direcao)
    """
    ano = int(input("Digite um novo ano para seu fusca: "))
    fusca.mudar_ano(ano)
    print(fusca)
