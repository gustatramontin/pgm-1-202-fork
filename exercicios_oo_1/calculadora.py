from functools import reduce

class Calculadora(object):

    def somar(self, a: int, b: int) -> int:
        return a+b

    def somar_n(self, a, *args):
        print(args)
        total = 0
        for numero in args:
            total += numero
        return total
    
    def sum(self, *nums):
       return reduce(lambda x,y: x+y, nums)


calc = Calculadora()
print(calc.somar_n(1, 2, 3, 4, 5, 6, 7, 10))
print(calc.somar_n(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
print(calc.sum(2,2,2))

