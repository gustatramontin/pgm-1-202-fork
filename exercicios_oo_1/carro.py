class Carro(object):

    """ pública - podemos acessar de qualquer local
        protegida - é uma variável que só pode ser acessada por códigos que estão no mesmo "diretório"/pacote
        privada - quando a variável só pode ser acessada dentro da classe que ela pertence
    """

    def __init__(self, nome: str, ano: int, placa: str, cor: str) -> None:
        self.velocidade = 0
        self.nome = nome
        self.__ano = ano
        self.placa = placa
        self.cor = cor

    def definir_ano(self, novo_ano) -> None:
        if novo_ano > 0:
            self.__ano = novo_ano
        else:
            self.__ano = 0

    def retornar_ano(self) -> int:
        return self.__ano
