from datetime import datetime


class Data:

    def __init__(self, dia: int, mes: int, ano: int) -> None:
        self.dia = dia
        self.mes = mes
        self.ano = ano

    def imprimir_data(self, dateType: str):
        types = {
            "br": "%d/%m/%Y",
            "us": "%m/%d/%Y"
        }

        if dateType not in types.keys():
            print('This is not a supported type')
            return False

        date = datetime(self.ano, self.mes, self.dia)

        output = date.strftime(types[dateType])

        return output
