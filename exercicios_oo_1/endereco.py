class Endereco(object):

    def __init__(self, rua: str, numero: int, cep: int, uf: str,
                 cidade: str) -> None:
        self.rua = rua
        self.numero = numero
        self.cep = cep
        self.uf = uf
        self.cidade = cidade

    def __eq__(self, end) -> bool:
        return self.rua == end.rua

    def __add__(self, end) -> str:
        return self.rua + end.rua

    def __repr__(self) -> str:
        return "Rua: " + self.rua + ", Número: " + str(self.numero)


casa1 = Endereco("Sem nome", 0, 132132, "SC", "Blumenau")
casa2 = Endereco("Sem nome", 0, 132132, "SC", "Blumenau")
#print(casa1 == casa2)
