from endereco import Endereco


class Pessoa(object):

    def __init__(self, nome: str, idade: int, end: Endereco) -> None:
        self.nome = nome
        self.idade = idade
        self.end = end
