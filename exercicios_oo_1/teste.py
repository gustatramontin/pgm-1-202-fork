class Calculadora(object):

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def soma(self):
        return self.a + self.b

    def soma_n(self, *args):
        return args

    def subtrai(self):
        return self.a - self.b

    def multiplica(self):
        return self.a * self.b

    def divide(self):
        return self.a / self.b


c = Calculadora(2, 2)
print("Soma: ", c.soma())
print("Soma N numeros: ", c.soma_n(1))
print("Subtração: ", c.subtrai())
print("Multiplicação: ", c.multiplica())
print("Divisão: ", c.divide())
